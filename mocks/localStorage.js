let cachedData = {}

global.localStorage = {
  getItem: jest.fn((key) => {
    return cachedData[key]
  }),
  setItem: jest.fn((key, data) => {
    cachedData[key] = data
  }),
  clear: jest.fn(() => {
    cachedData = {}
  })
}
