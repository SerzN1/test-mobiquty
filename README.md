#Test app for Mobiquity

### Description
Simple version of React/Redux/MaterialUI application without async middleware

### Live preview
[Bitbucket static pages example](https://serzn1.bitbucket.io)

### Fast start
1 `yarn install` - npm modules installation

2 `yarn start` - local server will be opened at hhtp://localhost:3000

3 `yarn test --watch` - testing application

4 `yarn lint:watch` - linting

5 `yarn build` - bundle application

### TODO
TS or Flow support
