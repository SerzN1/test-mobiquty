import React from 'react'
import { shallow } from 'enzyme'
import App from './App'
import configureMockStore from 'redux-mock-store'

const middlewares = []
const mockStore = configureMockStore(middlewares)

describe('<App />', () => {
  it('renders without crashing', () => {
    const initialState = {}
    const store = mockStore(initialState)
    shallow(<App store={store} />)
  })
})
