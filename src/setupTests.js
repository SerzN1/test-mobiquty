import Enzyme from 'enzyme'
import EnzymeAdapter from 'enzyme-adapter-react-16'
import '../mocks/localStorage'

Enzyme.configure({ adapter: new EnzymeAdapter() })
