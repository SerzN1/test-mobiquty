import React from 'react'
import ReactDOM from 'react-dom'
import {createStore} from 'redux'
import 'typeface-roboto'
import './index.css'
import App from './App'
import rootReducer from './ducks'
import registerServiceWorker from './registerServiceWorker'

const store = createStore(
  rootReducer,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
)

ReactDOM.render(<App store={store} />, document.getElementById('root'))
registerServiceWorker()
