import { generateYearsArray } from './utils/yearUtils'

export const YEAR_START = 2008
export const YEAR_END = 2015

export const YEARS = generateYearsArray(YEAR_START, YEAR_END)
