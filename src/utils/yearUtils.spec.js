import { generateYearsArray } from './yearUtils'

describe('utils/yearUtils', () => {
  describe('generateYearsArray', () => {
    it('should generate ASC array between selected years', () => {
      const start = 2012
      const end = 2015
      expect(generateYearsArray(start, end)).toEqual([2012, 2013, 2014, 2015])
    })
    it('should generate ASC array for bigger start date', () => {
      const start = 2015
      const end = 2012
      expect(generateYearsArray(start, end)).toEqual([2012, 2013, 2014, 2015])
    })
  })
})
