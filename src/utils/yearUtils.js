export function generateYearsArray(start, end) {
  if (start > end) {
    const temp = end
    end = start
    start = temp
  }
  const years = []
  for (let i = start; i <= end; i += 1) {
    years.push(i)
  }
  return years
}
