export function parseChampions(dataArray) {
  return dataArray
    .map(data => data.MRData.StandingsTable.StandingsLists[0])
    .map(data => {
      const standing = data.DriverStandings[0]
      const driverData = parseDriverApiData(standing.Driver)
      const team = standing.Constructors
        .map(constructor => constructor.name)
        .join(', ')

      return {
        ...driverData,
        season: data.season,
        team
      }
    })
}

export function parseWinners(data) {
  return data.MRData.RaceTable.Races
    .map(race => {
      const driverData = parseDriverApiData(race.Results[0].Driver)
      return {
        ...driverData,
        round: race.round,
        raceName: race.raceName
      }
    })
}

export function parseDriverApiData(driverApiData) {
  return {
    driver: `${driverApiData.givenName} ${driverApiData.familyName}`,
    driverId: driverApiData.driverId
  }
}
