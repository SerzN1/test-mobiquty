import { parseChampions, parseWinners, parseDriverApiData } from './parseUtils'

describe('utils/parseUtils', () => {
  describe('parseChampions', () => {
    it('should generate array between selected years', () => {
      /* eslint-disable */
      const championResponse1 = {"MRData":{"xmlns":"http:\/\/ergast.com\/mrd\/1.4","series":"f1","url":"http://ergast.com/api/f1/2008/driverstandings/1.json","limit":"30","offset":"0","total":"1","StandingsTable":{"season":"2008","driverStandings":"1","StandingsLists":[{"season":"2008","round":"18","DriverStandings":[{"position":"1","positionText":"1","points":"98","wins":"5","Driver":{"driverId":"hamilton","permanentNumber":"44","code":"HAM","url":"http:\/\/en.wikipedia.org\/wiki\/Lewis_Hamilton","givenName":"Lewis","familyName":"Hamilton","dateOfBirth":"1985-01-07","nationality":"British"},              "Constructors":[{"constructorId":"mclaren","url":"http:\/\/en.wikipedia.org\/wiki\/McLaren","name":"McLaren","nationality":"British"}]}]}]}}}
      const championResponse2 = {"MRData":{"xmlns":"http:\/\/ergast.com\/mrd\/1.4","series":"f1","url":"http://ergast.com/api/f1/2009/driverstandings/1.json","limit":"30","offset":"0","total":"1","StandingsTable":{"season":"2009","driverStandings":"1","StandingsLists":[{"season":"2009","round":"17","DriverStandings":[{"position":"1","positionText":"1","points":"95","wins":"6","Driver":{"driverId":"button","permanentNumber":"22","code":"BUT","url":"http:\/\/en.wikipedia.org\/wiki\/Jenson_Button","givenName":"Jenson","familyName":"Button","dateOfBirth":"1980-01-19","nationality":"British"},              "Constructors":[{"constructorId":"brawn","url":"http:\/\/en.wikipedia.org\/wiki\/Brawn_GP","name":"Brawn","nationality":"British"}]}]}]}}}
      /* eslint-enable */
      expect(parseChampions([championResponse1, championResponse2]))
        .toEqual([
          {
            'driver': 'Lewis Hamilton',
            'driverId': 'hamilton',
            'season': '2008',
            'team': 'McLaren'
          },
          {
            'driver': 'Jenson Button',
            'driverId': 'button',
            'season': '2009',
            'team': 'Brawn'
          }
        ])
    })
  })
  describe('parseWinners', () => {
    it('should generate array between selected years', () => {
      /* eslint-disable */
      const winnerResponse = {"MRData":{"xmlns":"http:\/\/ergast.com\/mrd\/1.4","series":"f1","url":"http://ergast.com/api/f1/2013/results/1.json","limit":"30","offset":"0","total":"19","RaceTable":{"season":"2013","position":"1","Races":[{"season":"2013","round":"1","url":"http:\/\/en.wikipedia.org\/wiki\/2013_Australian_Grand_Prix","raceName":"Australian Grand Prix","Circuit":{"circuitId":"albert_park","url":"http://en.wikipedia.org/wiki/Melbourne_Grand_Prix_Circuit","circuitName":"Albert Park Grand Prix Circuit","Location":{"lat":"-37.8497","long":"144.968","locality":"Melbourne","country":"Australia"}},"date":"2013-03-17","time":"06:00:00Z","Results":[{"number":"7","position":"1","positionText":"1","points":"25","Driver":{"driverId":"raikkonen","permanentNumber":"7","code":"RAI","url":"http:\/\/en.wikipedia.org\/wiki\/Kimi_R%C3%A4ikk%C3%B6nen","givenName":"Kimi","familyName":"Räikkönen","dateOfBirth":"1979-10-17","nationality":"Finnish"},"Constructor":{"constructorId":"lotus_f1","url":"http:\/\/en.wikipedia.org\/wiki\/Lotus_F1","name":"Lotus F1","nationality":"British"},"grid":"7","laps":"58","status":"Finished","Time":{"millis":"5403225","time":"1:30:03.225"},"FastestLap":{"rank":"1","lap":"56","Time":{"time":"1:29.274"},"AverageSpeed":{"units":"kph","speed":"213.845"}}}]},{"season":"2013","round":"2","url":"http:\/\/en.wikipedia.org\/wiki\/2013_Malaysian_Grand_Prix","raceName":"Malaysian Grand Prix","Circuit":{"circuitId":"sepang","url":"http://en.wikipedia.org/wiki/Sepang_International_Circuit","circuitName":"Sepang International Circuit","Location":{"lat":"2.76083","long":"101.738","locality":"Kuala Lumpur","country":"Malaysia"}},"date":"2013-03-24","time":"08:00:00Z","Results":[{"number":"1","position":"1","positionText":"1","points":"25","Driver":{"driverId":"vettel","permanentNumber":"5","code":"VET","url":"http:\/\/en.wikipedia.org\/wiki\/Sebastian_Vettel","givenName":"Sebastian","familyName":"Vettel","dateOfBirth":"1987-07-03","nationality":"German"},"Constructor":{"constructorId":"red_bull","url":"http:\/\/en.wikipedia.org\/wiki\/Red_Bull_Racing","name":"Red Bull","nationality":"Austrian"},"grid":"1","laps":"56","status":"Finished","Time":{"millis":"5936681","time":"1:38:56.681"},"FastestLap":{"rank":"3","lap":"45","Time":{"time":"1:40.446"},"AverageSpeed":{"units":"kph","speed":"198.661"}}}]}]}}}
      /* eslint-enable */
      expect(parseWinners(winnerResponse))
        .toEqual([
          {
            'driver': 'Kimi Räikkönen',
            'driverId': 'raikkonen',
            'raceName': 'Australian Grand Prix',
            'round': '1'
          },
          {
            'driver': 'Sebastian Vettel',
            'driverId': 'vettel',
            'raceName': 'Malaysian Grand Prix',
            'round': '2'
          }
        ])
    })
  })
  describe('parseDriverApiData', () => {
    it('should generate array between selected years', () => {
      /* eslint-disable */
      const driverResponse = {
        "driverId": "hamilton",
        "permanentNumber": "44",
        "code": "HAM",
        "url": "http://en.wikipedia.org/wiki/Lewis_Hamilton",
        "givenName": "Lewis",
        "familyName": "Hamilton",
        "dateOfBirth": "1985-01-07",
        "nationality": "British"
      }
      /* eslint-enable */
      expect(parseDriverApiData(driverResponse))
        .toEqual({
          driver: 'Lewis Hamilton',
          driverId: 'hamilton'
        })
    })
  })
})
