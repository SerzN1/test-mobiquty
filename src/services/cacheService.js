export function setCachedData(key, data) {
  if (!data) return
  window.localStorage.setItem(key, JSON.stringify(data))
  return data
}

export function getCachedData(key) {
  const data = window.localStorage.getItem(key)
  try {
    return JSON.parse(data)
  } catch (e) {
    return data
  }
}
