/* global localStorage */
import { setCachedData, getCachedData } from './cacheService'

describe('services/cacheService', () => {
  afterEach(() => {
    localStorage.setItem.mockClear()
    localStorage.getItem.mockClear()
    localStorage.clear.mockClear()
  })

  describe('setCachedData', () => {
    it('should save data to cache', () => {
      const key = 'cacheKey'
      const data = { example: 42 }

      setCachedData(key, data)
      expect(localStorage.setItem).toHaveBeenCalledWith(key, JSON.stringify(data))
    })
  })

  describe('getCachedData', () => {
    beforeEach(() => {
      const key = 'cacheKey'
      const data = { example: 42 }
      localStorage.setItem(key, JSON.stringify(data))
      localStorage.getItem.mockClear()
    })

    it('should return parsed cached data', () => {
      const key = 'cacheKey'
      expect(getCachedData(key)).toEqual({ example: 42 })
    })
    it('should return undefined for missing keys', () => {
      const key = 'missingKey'
      expect(getCachedData(key)).toBeUndefined()
    })
  })
})
