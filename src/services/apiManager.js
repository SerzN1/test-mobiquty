import { parseChampions, parseWinners } from '../utils/parseUtils'
import { YEARS } from '../constant'
import { getCachedData, setCachedData } from './cacheService'

export function generateApiUrl(urlPath) {
  return `//ergast.com${urlPath}.json`
}

export function getChampions() {
  const championRequests = YEARS
    .map(year => generateApiUrl(`/api/f1/${year}/driverStandings/1`))
    .map(yearUrl => apiGet(yearUrl))

  return Promise.all(championRequests).then(parseChampions)
}

export function getWinners(year) {
  const url = generateApiUrl(`/api/f1/${year}/results/1`)
  return apiGet(url).then(parseWinners)
}

export const apiGet = (url, opts = {}) => {
  const key = JSON.stringify({
    url,
    ...opts
  })
  const data = getCachedData(key)
  if (data) {
    return Promise.resolve(data)
  }
  return fetch(url, opts)
    .then(res => res.json())
    .then(data => setCachedData(key, data))
}
