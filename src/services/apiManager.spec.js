import { generateApiUrl, getChampions, getWinners, apiGet } from './apiManager'
import { parseChampions, parseWinners } from '../utils/parseUtils'
import { getCachedData, setCachedData } from './cacheService'
import { YEARS } from '../constant'

jest.mock('../utils/parseUtils', () => ({
  parseChampions: jest.fn(data => data),
  parseWinners: jest.fn(data => data),
  parseDriverApiData: jest.fn(data => data)
}))
jest.mock('./cacheService', () => ({
  getCachedData: jest.fn(key => {
    const validKey = JSON.stringify({ url: 'VALID_KEY' })
    if (key === validKey) {
      return { data: 42 }
    }
  }),
  setCachedData: jest.fn((key, data) => data)
}))

describe('services/apiManager', () => {
  afterEach(() => {
    fetch.mockClear()
    setCachedData.mockClear()
  })

  describe('generateApiUrl', () => {
    it('should generate url from json data', () => {
      const apiPath = '/api/1'
      expect(generateApiUrl(apiPath)).toEqual('//ergast.com/api/1.json')
    })
  })

  describe('getChampions', () => {
    it('should request champion for each year and parse response', async () => {
      fetch.mockResponse(JSON.stringify([{ champion: 1 }]))
      const apiUrl = '//ergast.com/api/f1/2015/driverStandings/1.json'
      const parserResponse = YEARS.map(() => ([{ champion: 1 }]))
      const championsResponse = await getChampions()
      expect(fetch).toHaveBeenCalledTimes(YEARS.length)
      expect(fetch).toHaveBeenCalledWith(apiUrl, {})
      expect(parseChampions).toHaveBeenCalledWith(parserResponse)
      expect(championsResponse).toEqual(parserResponse)
    })
  })

  describe('getWinners', () => {
    it('should request winners for the selected year and parse response', async () => {
      fetch.mockResponseOnce(JSON.stringify({ winners: [] }))
      const apiUrl = '//ergast.com/api/f1/2015/results/1.json'
      const parsedResponse = { winners: [] }
      const winnersResponse = await getWinners(2015)
      expect(fetch).toHaveBeenCalledWith(apiUrl, {})
      expect(parseWinners).toHaveBeenCalledWith(parsedResponse)
      expect(winnersResponse).toEqual(parsedResponse)
    })
  })

  describe('apiGet', () => {
    it('should return data from cache instead request', async () => {
      const apiPath = 'VALID_KEY'
      const dataFromCache = await apiGet(apiPath)
      const storageKey = JSON.stringify({ url: apiPath })
      expect(getCachedData).toHaveBeenCalledWith(storageKey)
      expect(dataFromCache).toEqual({ data: 42 })
    })
    it('should create request and cache them', async () => {
      fetch.mockResponseOnce(JSON.stringify({ movie: 43 }))
      const apiPath = '/api/1'
      const storageKey = JSON.stringify({ url: apiPath })
      const responseFromServer = await apiGet(apiPath)
      expect(setCachedData).toHaveBeenCalledWith(storageKey, { movie: 43 })
      expect(responseFromServer).toEqual({ movie: 43 })
    })
  })
})
