import reducer, {
  loadWinners,
  doneWinners,
  failWinners,
  LOAD_WINNERS,
  DONE_WINNERS,
  FAIL_WINNERS
} from './winners'

describe('ducks/winner', () => {
  describe('actions', () => {
    it('should generate load action for specific season', () => {
      const season = 2012
      expect(loadWinners(season)).toEqual({
        type: LOAD_WINNERS,
        payload: 2012
      })
    })
    it('should generate load success action for specific season', () => {
      const payload = {
        season: 2012,
        data: {
          winner: 'Hamilton',
          track: 'Hockenheim Ring'
        }
      }
      expect(doneWinners(payload)).toEqual({
        type: DONE_WINNERS,
        payload
      })
    })
    it('should generate load fail action for specific season', () => {
      const season = 2012
      expect(failWinners(season)).toEqual({
        type: FAIL_WINNERS,
        payload: 2012
      })
    })
  })

  describe('reducer', () => {
    let initialState
    const winnerInitialState = {
      loading: true,
      error: false,
      list: []
    }

    beforeEach(() => {
      initialState = {
        seasons: {}
      }
    })

    it('should not react on unsuitable actions', () => {
      expect(reducer(initialState, {}))
        .toEqual(initialState)
    })

    it('should update state after LOAD_WINNERS action', () => {
      const loadAction = {
        type: LOAD_WINNERS,
        payload: 2012
      }
      expect(reducer(initialState, loadAction))
        .toEqual({
          seasons: {
            ...initialState.seasons,
            2012: {
              ...winnerInitialState
            }
          }
        })
    })

    it('should update state after DONE_WINNERS action', () => {
      const doneAction = {
        type: DONE_WINNERS,
        payload: {
          season: 2012,
          data: ['3', '2', '1']
        }
      }
      expect(reducer(initialState, doneAction))
        .toEqual({
          seasons: {
            ...initialState.seasons,
            2012: {
              list: ['3', '2', '1'],
              loading: false,
              error: false
            }
          }
        })
    })

    it('should update state after FAIL_WINNERS action', () => {
      const loadAction = {
        type: FAIL_WINNERS,
        payload: 2012
      }
      expect(reducer(initialState, loadAction))
        .toEqual({
          seasons: {
            ...initialState.seasons,
            2012: {
              ...winnerInitialState,
              error: true,
              loading: false
            }
          }
        })
    })
  })
})
