export const LOAD_WINNERS = 'winners/LOAD'
export const DONE_WINNERS = 'winners/DONE'
export const FAIL_WINNERS = 'winners/FAIL'

const winnerInitialState = {
  loading: true,
  error: false,
  list: []
}

const initialState = {
  seasons: {}
}

export function loadWinners(payload) {
  return {
    type: LOAD_WINNERS,
    payload
  }
}
export function doneWinners(payload) {
  return {
    type: DONE_WINNERS,
    payload
  }
}
export function failWinners(payload) {
  return {
    type: FAIL_WINNERS,
    payload
  }
}

export default function reducer(state = initialState, {type, payload}) {
  switch (type) {
    case LOAD_WINNERS:
      return {
        seasons: {
          ...state.seasons,
          [payload]: {...winnerInitialState}
        }
      }

    case DONE_WINNERS:
      return {
        seasons: {
          ...state.seasons,
          [payload.season]: {
            list: payload.data,
            loading: false,
            error: false
          }
        }
      }

    case FAIL_WINNERS:
      return {
        seasons: {
          ...state.seasons,
          [payload]: {
            ...winnerInitialState,
            loading: false,
            error: true
          }
        }
      }

    default:
      return state
  }
}
