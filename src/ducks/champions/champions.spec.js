import reducer, {
  loadChampions,
  doneChampions,
  failChampions,
  expandChampions,
  LOAD_CHAMPIONS,
  DONE_CHAMPIONS,
  FAIL_CHAMPIONS,
  EXPAND_CHAMPIONS
} from './champions'

describe('ducks/champions', () => {
  describe('actions', () => {
    it('should generate load action', () => {
      expect(loadChampions()).toEqual({
        type: LOAD_CHAMPIONS
      })
    })
    it('should generate load success action with payload', () => {
      expect(doneChampions(42)).toEqual({
        type: DONE_CHAMPIONS,
        payload: 42
      })
    })
    it('should generate load fail action', () => {
      expect(failChampions()).toEqual({
        type: FAIL_CHAMPIONS
      })
    })
    it('should generate load action', () => {
      const expandedSeason = 2012
      expect(expandChampions(expandedSeason)).toEqual({
        type: EXPAND_CHAMPIONS,
        payload: expandedSeason
      })
    })
  })

  describe('reducer', () => {
    let initialState

    beforeEach(() => {
      initialState = {
        loading: false,
        list: [],
        error: false,
        expanded: undefined
      }
    })

    it('should not react on unsuitable actions', () => {
      expect(reducer(initialState, {}))
        .toEqual(initialState)
    })

    it('should update state after LOAD_CHAMPIONS action', () => {
      const loadAction = {
        type: LOAD_CHAMPIONS
      }
      expect(reducer(initialState, loadAction))
        .toEqual({
          ...initialState
        })
    })

    it('should update state after DONE_CHAMPIONS action', () => {
      const doneAction = {
        type: DONE_CHAMPIONS,
        payload: ['1', '2', '0']
      }
      expect(reducer(initialState, doneAction))
        .toEqual({
          list: ['1', '2', '0'],
          error: false,
          loading: false,
          expanded: undefined
        })
    })

    it('should update state after FAIL_CHAMPIONS action', () => {
      const failAction = {
        type: FAIL_CHAMPIONS
      }
      expect(reducer(initialState, failAction))
        .toEqual({
          ...initialState,
          error: true,
          loading: false
        })
    })

    it('should update state after EXPAND_CHAMPIONS action', () => {
      const expandAction = {
        type: EXPAND_CHAMPIONS,
        payload: 42
      }
      expect(reducer(initialState, expandAction))
        .toEqual({
          ...initialState,
          expanded: 42
        })
    })

    it('should reset expand state after doubled EXPAND_CHAMPIONS action', () => {
      const state = {
        ...initialState,
        expanded: 42
      }
      const expandAction = {
        type: EXPAND_CHAMPIONS,
        payload: 42
      }
      expect(reducer(state, expandAction))
        .toEqual({
          ...state,
          expanded: undefined
        })
    })
  })
})
