export const LOAD_CHAMPIONS = 'champions/LOAD'
export const DONE_CHAMPIONS = 'champions/DONE'
export const FAIL_CHAMPIONS = 'champions/FAIL'
export const EXPAND_CHAMPIONS = 'champions/EXPAND'

const initialState = {
  loading: true,
  list: [],
  error: false,
  expanded: undefined
}

export function loadChampions() {
  return {
    type: LOAD_CHAMPIONS
  }
}
export function doneChampions(payload) {
  return {
    type: DONE_CHAMPIONS,
    payload
  }
}
export function failChampions() {
  return {
    type: FAIL_CHAMPIONS
  }
}
export function expandChampions(payload) {
  return {
    type: EXPAND_CHAMPIONS,
    payload
  }
}

export default function reducer(state = initialState, {type, payload}) {
  switch (type) {
    case LOAD_CHAMPIONS:
      return {
        ...state,
        loading: false,
        error: false
      }

    case DONE_CHAMPIONS:
      return {
        ...state,
        list: payload,
        loading: false,
        error: false
      }

    case FAIL_CHAMPIONS:
      return {
        ...state,
        loading: false,
        error: true
      }

    case EXPAND_CHAMPIONS:
      return {
        ...state,
        expanded: state.expanded !== payload ? payload : undefined
      }

    default:
      return state
  }
}
