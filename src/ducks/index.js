import {combineReducers} from 'redux'
import champions from './champions/champions'
import winners from './winners/winners'

const rootReducer = combineReducers({
  champions,
  winners
})

export default rootReducer
