import React from 'react'
import Header from './Header'
import { createShallow, createRender } from 'material-ui/test-utils'
import { YEAR_END, YEAR_START } from '../../constant'

describe('<Header />', () => {
  let shallow
  let render

  beforeAll(() => {
    shallow = createShallow({dive: true})
    render = createRender()
  })

  it('should renders without crashing', () => {
    shallow(<Header />)
  })

  it('should render AppBar inside', () => {
    const component = shallow(<Header />)
    const appBar = component.find('WithStyles(AppBar)')
    expect(appBar).toHaveLength(1)
    expect(appBar.prop('position')).toBe('static')
  })

  it('should render start and end years', () => {
    const component = render(<Header />)
    expect(component.text().includes(YEAR_START)).toBeTruthy()
    expect(component.text().includes(YEAR_END)).toBeTruthy()
  })
})
