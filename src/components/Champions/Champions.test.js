import React from 'react'
import { shallow, mount } from 'enzyme'
import { Provider } from 'react-redux'
import configureMockStore from 'redux-mock-store'
import Champions from './Champions'
import { getChampions } from '../../services/apiManager'
import { EXPAND_CHAMPIONS, LOAD_CHAMPIONS } from '../../ducks/champions/champions'

const middlewares = []
const mockStore = configureMockStore(middlewares)

jest.mock('material-ui/Progress/CircularProgress', () => 'progress')
jest.mock('../Winners/Winners', () => 'winners')
jest.mock('../../services/apiManager', () => ({
  getChampions: jest.fn(() => {
    return Promise.resolve()
  })
}))

describe('<Champions />', () => {
  it('should renders without crashing', () => {
    const initialState = {}
    const store = mockStore(initialState)
    shallow(
      <Provider store={store}>
        <Champions />
      </Provider>
    )
  })

  it('should render preloader when loading', () => {
    const initialState = {
      champions: {
        loading: true,
        list: [],
        error: false,
        expanded: undefined
      }
    }
    const store = mockStore(initialState)
    const wrapper = mount(
      <Provider store={store}>
        <Champions />
      </Provider>
    )
    expect(wrapper.find('progress')).toHaveLength(1)
  })

  it('should call loadChampions action and getChampions api call on mount', () => {
    const initialState = {
      champions: {
        loading: true,
        list: [],
        error: false,
        expanded: undefined
      }
    }
    const store = mockStore(initialState)
    mount(
      <Provider store={store}>
        <Champions />
      </Provider>
    )
    const actions = store.getActions()
    expect(actions).toEqual([{ type: LOAD_CHAMPIONS }])
    expect(getChampions).toHaveBeenCalled()
  })

  it('should render champions list when loaded', () => {
    const initialState = {
      champions: {
        loading: false,
        list: [{
          season: '2012',
          driver: 'Vettel',
          driverId: 'vettel',
          team: 'Ferrari'
        }, {
          season: '2013',
          driver: 'Vettel',
          driverId: 'vettel',
          team: 'Scuderia'
        }],
        error: false,
        expanded: undefined
      }
    }
    const store = mockStore(initialState)
    const wrapper = mount(
      <Provider store={store}>
        <Champions />
      </Provider>
    )
    const championRows = wrapper.find('ExpansionPanel')
    expect(championRows).toHaveLength(2)
    expect(championRows.at(0).text().includes('Ferrari')).toBeTruthy()
    expect(championRows.at(1).text().includes('Scuderia')).toBeTruthy()
  })

  it('should call expandChampions action when champion item clicked', () => {
    const initialState = {
      champions: {
        loading: false,
        list: [{
          season: '2012',
          driver: 'Vettel',
          driverId: 'vettel',
          team: 'Ferrari'
        }],
        error: false,
        expanded: undefined
      }
    }
    const store = mockStore(initialState)
    const wrapper = mount(
      <Provider store={store}>
        <Champions />
      </Provider>
    )
    wrapper.find('ExpansionPanelSummary').simulate('click')
    const actions = store.getActions()
    const expectedAction = { type: EXPAND_CHAMPIONS, payload: '2012' }
    const storedExpandActions = actions
      .filter(action => action.type === expectedAction.type)
    expect(storedExpandActions).toHaveLength(1)
    expect(storedExpandActions[0]).toEqual(expectedAction)
  })

  it('should render expanded winners list if selected', () => {
    const initialState = {
      champions: {
        loading: false,
        list: [{
          season: '2012',
          driver: 'Vettel',
          driverId: 'vettel',
          team: 'Ferrari'
        }],
        error: false,
        expanded: '2012'
      }
    }
    const store = mockStore(initialState)
    const wrapper = mount(
      <Provider store={store}>
        <Champions />
      </Provider>
    )
    expect(wrapper.find('winners[expanded=true]')).toHaveLength(1)
  })
})
