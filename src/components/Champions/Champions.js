import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import ExpansionPanel, {
  ExpansionPanelDetails,
  ExpansionPanelSummary
} from 'material-ui/ExpansionPanel'
import Typography from 'material-ui/Typography'
import ExpandMoreIcon from 'material-ui-icons/ExpandMore'
import Winners from '../Winners/Winners'
import { withStyles } from 'material-ui/styles/index'
import { getChampions } from '../../services/apiManager'
import {
  doneChampions, expandChampions, failChampions, loadChampions
} from '../../ducks/champions/champions'
import { CircularProgress } from 'material-ui/Progress/'

const styles = theme => ({
  root: theme.mixins.gutters({
    paddingTop: 16,
    paddingBottom: 16
  }),
  progress: {
    margin: '0 auto'
  },
  season: {
    fontSize: theme.typography.pxToRem(15),
    flexBasis: '50px',
    flexShrink: 0
  },
  name: {
    fontSize: theme.typography.pxToRem(15),
    flexBasis: '45%',
    flexShrink: 0
  },
  team: {
    fontSize: theme.typography.pxToRem(15),
    flexBasis: '45%',
    flexShrink: 0
  }
})

class Champions extends Component {
  constructor(props) {
    super(props)

    this.handleExpand = this.handleExpand.bind(this)
    this.renderChampion = this.renderChampion.bind(this)
  }
  componentWillMount() {
    this.props.loadChampions()
    getChampions()
      .then(data => this.props.doneChampions(data))
      .catch(err => this.props.failChampions(err))
  }

  handleExpand(championId) {
    this.props.expandChampions(championId)
  }

  render() {
    const {classes, loading} = this.props
    return loading
      ? <CircularProgress className={classes.progress} color="secondary" />
      : this.props.champions.map(this.renderChampion)
  }

  renderChampion({ season, driver, driverId, team }) {
    const {classes, expanded} = this.props
    const isExpanded = expanded === season

    return (
      <ExpansionPanel
        key={season}
        expanded={isExpanded}
        onChange={() => this.handleExpand(season)}
      >
        <ExpansionPanelSummary expandIcon={<ExpandMoreIcon/>}>
          <Typography className={classes.season}>{season}</Typography>
          <Typography className={classes.name}>{driver}</Typography>
          <Typography className={classes.team}>{team}</Typography>
        </ExpansionPanelSummary>
        <ExpansionPanelDetails>
          <Winners season={season} championId={driverId} expanded={isExpanded} />
        </ExpansionPanelDetails>
      </ExpansionPanel>
    )
  }
}

Champions.defaultProps = {
  champions: []
}

Champions.propTypes = {
  loading: PropTypes.bool.isRequired,
  champions: PropTypes.arrayOf(PropTypes.shape({
    season: PropTypes.string,
    driver: PropTypes.string,
    driverId: PropTypes.string,
    team: PropTypes.string
  })),
  classes: PropTypes.object.isRequired,
  expanded: PropTypes.string,
  doneChampions: PropTypes.func.isRequired,
  failChampions: PropTypes.func.isRequired,
  loadChampions: PropTypes.func.isRequired,
  expandChampions: PropTypes.func.isRequired
}

export function mapStateToProps(state) {
  return {
    champions: state.champions.list,
    expanded: state.champions.expanded,
    loading: state.champions.loading
  }
}

export default connect(
  mapStateToProps,
  {
    loadChampions,
    doneChampions,
    failChampions,
    expandChampions
  }
)(withStyles(styles)(Champions))
