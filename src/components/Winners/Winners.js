import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import Table, { TableBody, TableCell, TableHead, TableRow } from 'material-ui/Table'
import { getWinners } from '../../services/apiManager'
import { loadWinners, doneWinners, failWinners } from '../../ducks/winners/winners'
import { CircularProgress } from 'material-ui/Progress/'
import { withStyles } from 'material-ui'

const styles = ({
  progress: {
    margin: '0 auto'
  }
})

class Winners extends Component {
  componentWillMount() {
    if (this.props.expanded) {
      this.loadWinnersData()
    }
  }
  componentWillReceiveProps(nextProps) {
    if (!this.props.expanded && nextProps.expanded) {
      this.loadWinnersData()
    }
  }

  render() {
    const { classes, loading } = this.props
    return loading
      ? <CircularProgress className={classes.progress} color="secondary" />
      : this.renderWinnersTable()
  }

  renderWinnersTable() {
    return (
      <Table>
        <TableHead>
          <TableRow>
            <TableCell padding='checkbox'>#</TableCell>
            <TableCell>Race</TableCell>
            <TableCell>Winner</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {this.props.list.map(this.renderWinner)}
        </TableBody>
      </Table>
    )
  }

  renderWinner = (winner) => {
    const isChampion = winner.driverId === this.props.championId
    return (
      <TableRow key={winner.round} selected={isChampion}>
        <TableCell padding='checkbox'>{winner.round}</TableCell>
        <TableCell>{winner.raceName}</TableCell>
        <TableCell>
          {winner.driver}
          {isChampion && <span dangerouslySetInnerHTML={{__html: ' &#9812;'}} />}
        </TableCell>
      </TableRow>
    )
  };

  loadWinnersData() {
    this.props.loadWinners()
    getWinners(this.props.season)
      .then(res => this.props.doneWinners(res))
      .catch(err => this.props.failWinners(err))
  }
}

Winners.propTypes = {
  expanded: PropTypes.bool.isRequired,
  classes: PropTypes.object.isRequired,
  championId: PropTypes.string.isRequired,
  season: PropTypes.string.isRequired,
  list: PropTypes.arrayOf(PropTypes.shape({
    driverId: PropTypes.string,
    driver: PropTypes.string,
    round: PropTypes.string,
    raceName: PropTypes.string
  })),
  loading: PropTypes.bool.isRequired,
  error: PropTypes.bool.isRequired,
  loadWinners: PropTypes.func.isRequired,
  doneWinners: PropTypes.func.isRequired,
  failWinners: PropTypes.func.isRequired
}

function mapStateToProps(state, {season}) {
  return (localState) => {
    const seasonData = localState.winners.seasons[season]
    return ({
      list: (seasonData && seasonData.list) || [],
      loading: Boolean(seasonData && seasonData.loading),
      error: Boolean(seasonData && seasonData.error)
    })
  }
}

function mapDispatchToProps(dispatch, {season}) {
  return {
    loadWinners: () => dispatch(loadWinners(season)),
    doneWinners: (data) => {
      dispatch(doneWinners({data, season}))
    },
    failWinners: () => dispatch(failWinners(season))
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(styles)(Winners))
