import React from 'react'
import { shallow, mount } from 'enzyme'
import { Provider } from 'react-redux'
import configureMockStore from 'redux-mock-store'
import Winners from './Winners'
import { getWinners } from '../../services/apiManager'
import { LOAD_WINNERS } from '../../ducks/winners/winners'

const middlewares = []
const mockStore = configureMockStore(middlewares)

jest.mock('material-ui/Progress/CircularProgress', () => 'progress')
jest.mock('../../services/apiManager', () => ({
  getWinners: jest.fn(() => {
    return Promise.resolve()
  })
}))

describe('<Winners />', () => {
  it('should renders without crashing', () => {
    const initialState = {}
    const store = mockStore(initialState)
    shallow(
      <Provider store={store}>
        <Winners />
      </Provider>
    )
  })

  it('should render preloader when loading', () => {
    const initialState = {
      winners: {
        seasons: {
          2012: {
            list: [],
            loading: true,
            error: false
          }
        }
      }
    }
    const store = mockStore(initialState)
    const wrapper = mount(
      <Provider store={store}>
        <Winners season={'2012'} expanded={false} championId={'fettel'} />
      </Provider>
    )
    expect(wrapper.find('progress')).toHaveLength(1)
  })

  it('should render call loadWinners action and getWinners api when expanded', () => {
    const initialState = {
      winners: {
        seasons: {
          2012: {
            list: [],
            loading: true,
            error: false
          }
        }
      }
    }
    const store = mockStore(initialState)
    mount(
      <Provider store={store}>
        <Winners season={'2012'} expanded championId={'fettel'} />
      </Provider>
    )
    const actions = store.getActions()
    const expectedAction = { type: LOAD_WINNERS, payload: '2012' }
    expect(getWinners).toHaveBeenCalledWith('2012')
    expect(actions).toEqual([expectedAction])
  })

  it('should render winners', () => {
    const initialState = {
      winners: {
        seasons: {
          2012: {
            list: [{
              driverId: 'vettel',
              driver: 'Vettel',
              round: 'round12',
              raceName: 'Nurnburing ring'
            }, {
              driverId: 'hamilton',
              driver: 'Hamilton',
              round: 'round13',
              raceName: 'Spain'
            }],
            loading: false,
            error: false
          }
        }
      }
    }
    const store = mockStore(initialState)
    const wrapper = mount(
      <Provider store={store}>
        <Winners season={'2012'} expanded championId={'fettel'} />
      </Provider>
    )
    const winnerRow = wrapper.find('TableBody TableRow')
    const secondWinnerRow = winnerRow.at(1)
    expect(winnerRow).toHaveLength(2)
    expect(secondWinnerRow.text().includes('round13')).toBeTruthy()
    expect(secondWinnerRow.text().includes('Hamilton')).toBeTruthy()
    expect(secondWinnerRow.text().includes('Spain')).toBeTruthy()
    expect(secondWinnerRow.text().includes('♔')).toBeFalsy()
  })

  it('should render winners and mark champions as selected', () => {
    const initialState = {
      winners: {
        seasons: {
          2012: {
            list: [{
              driverId: 'vettel',
              driver: 'Vettel',
              round: 'round12',
              raceName: 'Nurnburing ring'
            }],
            loading: false,
            error: false
          }
        }
      }
    }
    const store = mockStore(initialState)
    const wrapper = mount(
      <Provider store={store}>
        <Winners season={'2012'} expanded championId={'vettel'} />
      </Provider>
    )
    const winnerRow = wrapper.find('TableBody TableRow[selected]')
    expect(winnerRow).toHaveLength(1)
    expect(winnerRow.text().includes('round12')).toBeTruthy()
    expect(winnerRow.text().includes('Vettel')).toBeTruthy()
    expect(winnerRow.text().includes('Nurnburing ring')).toBeTruthy()
    expect(winnerRow.text().includes('♔')).toBeTruthy()
  })
})
