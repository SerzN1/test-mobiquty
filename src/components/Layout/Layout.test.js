import React from 'react'
import Layout from './Layout'
import { createShallow } from 'material-ui/test-utils'

jest.mock('../Header/Header', () => 'Header')
jest.mock('../Champions/Champions', () => 'Champions')

describe('<Layout />', () => {
  let shallow

  beforeAll(() => {
    shallow = createShallow({dive: true})
  })

  it('should renders without crashing', () => {
    shallow(<Layout />)
  })

  it('should render Header and Champions as children', () => {
    const component = shallow(<Layout />)
    expect(component.find('Header')).toHaveLength(1)
    expect(component.find('Champions')).toHaveLength(1)
  })
})
