import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Header from '../Header/Header'
import Champions from '../Champions/Champions'
import { withStyles } from 'material-ui/styles'

const styles = theme => ({
  root: theme.mixins.gutters({
    paddingTop: 16,
    paddingBottom: 16,
    maxWidth: 800,
    margin: '0 auto'
  })
})

class Layout extends Component {
  render() {
    const { classes } = this.props

    return (
      <div>
        <Header />
        <div className={classes.root}>
          <Champions />
        </div>
      </div>
    )
  }
}

Layout.propTypes = {
  classes: PropTypes.object
}

export default withStyles(styles)(Layout)
