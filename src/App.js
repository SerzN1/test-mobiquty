import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Provider } from 'react-redux'
import Layout from './components/Layout/Layout'

class App extends Component {
  render() {
    return (
      <Provider store={this.props.store}>
        <Layout />
      </Provider>
    )
  }
}

App.propTypes = {
  store: PropTypes.object.isRequired
}

export default App
